package com.unomic.cnc.communication.params;

public class RepLoginParam
{
	private int polling;
	private int path_num;
	private int result;
	
	public int getPolling()
	{
		return polling;
	}
	public void setPolling(int polling)
	{
		this.polling = polling;
	}
	public int getPath_num()
	{
		return path_num;
	}
	public void setPath_num(int path_num)
	{
		this.path_num = path_num;
	}
	public int getResult()
	{
		return result;
	}
	public void setResult(int result)
	{
		this.result = result;
	}
}
