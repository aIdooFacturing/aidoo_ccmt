<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<title><spring:message  code="f_m_l_check"></spring:message></title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<style type="text/css">
	body{
		overflow :hidden;
	}
</style>
<script type="text/javascript">
var shopId = 1;

var save_type = "init"

function getGroup(){
	var url = "${ctxPath}/chart/getPrdNoList.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			json = data.dataList;
			
			var option = "";
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
			});
			
			$("#prdNo").html(option).change(getDvcListByPrdNo);
			 
			$("#chkTy").html("<option value='ALL' >전체</option>" + getCheckType());
			
			getDvcListByPrdNo();
		}
	});
};

function getDvcListByPrdNo(){
	var url = "${ctxPath}/chart/getDvcListByPrdNo.do";
	var param = "prdNo=" + $("#prdNo").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var option = "";
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.dvcId + "'>" + decode(data.name) + "</option>"; 
			});
			
			$("#dvcId").html(option).val(json[0].dvcId);
			getCheckList();
		}
	});
};


function decode(str){
	return decodeURIComponent(str).replace(/\+/gi, " ")
};


function getCheckType(){
	var url = "${ctxPath}/chart/getCheckType.do";
	var param = "codeGroup=INSPPNT";
	
	var option = "";
	
	$.ajax({
		url : url,
		data : param,
		async : false,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.id + "'>" + decode(data.codeName) + "</option>"; 
			});
			
			chkTy = "<select>" + option + "</select>";
		}
	});
	
	return option;
};

$(function(){
	getGroup();
	$(".excel").click(csvSend);
	setEl();
	setDate();
	
	
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	$(".menu").click(goReport);
	$("#panel_table td").addClass("unSelected_menu");
	$("#menu102").removeClass("unSelected_menu");
	$("#menu102").addClass("selected_menu");
});

function chkFaulty(el, ty){
	if(ty=="select"){
		var val = $(el).val();
		
		if(val==2){
			$(el).parent("Td").parent("tr").css("background-color" , "red");
		}else{
			if($(el).parent("Td").parent("tr").hasClass("row1")){
				$(el).parent("Td").parent("tr").css("background-color" , "#222222");	 
			}else{
				$(el).parent("Td").parent("tr").css("background-color" , "#323232");
			}
		};	
	}else{
		var min = Number($(el).parent("td").parent("tr").children("td:nth(7)").html());
		var max = Number($(el).parent("td").parent("tr").children("td:nth(8)").html());
		var val = Number($(el).val());
		
		if(val>max || val < min){
			$(el).parent("Td").parent("tr").css("background-color" , "red");
		}else{
			if($(el).parent("Td").parent("tr").hasClass("row1")){
				$(el).parent("Td").parent("tr").css("background-color" , "#222222");	 
			}else{
				$(el).parent("Td").parent("tr").css("background-color" , "#323232");
			}
		}
	}
};

var className = "";
var classFlag = true;
var result = "<select style='font-size:" + getElSize(40) + "' onchange='chkFaulty(this, \"select\")'><option value='0' >선택</option><option value='1'>${ok}</option><option value='2'>${faulty}</option></select>";
function getCheckList(){
	classFlag = true;
	var url = "${ctxPath}/chart/getCheckList.do";
	var sDate = $(".date").val();
	
	var param = "prdNo=" + $("#prdNo").val() + 
				"&chkTy=" + $("#chkTy").val() +
				"&checkCycle=" + $("#checkCycle").val() + 
				"&date=" + sDate + 
				"&ty=" + $("#workTime").val();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var tr = "<tbody>";
					
			alarmList = [];
			$(json).each(function(idx, data){
				if(data.seq!="."){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					var attrTy;
					if(data.attrTy==1){
						attrTy = "정성검사";
					}else{
						attrTy = "정량검사";
					};
				
					var date;
					if(data.date==""){
						date = $(".date").val();
					}else{
						date = data.date;
					};
					
					var resultVal, resultVal2, resultVal3, resultVal4;
					if(data.attrTy==1){
						resultVal = resultVal2 = resultVal3 = resultVal4 = result;
					}else{
						resultVal = "<input type='text' value='" + data.result + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
						resultVal2 = "<input type='text' value='" + data.result2 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
						resultVal3 = "<input type='text' value='" + data.result3 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
						resultVal4 = "<input type='text' value='" + data.result4 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
					};
					
					tr = "<tr class='contentTr " + className + "' id='tr" + data.id + "'>" +
								"<td id='t" + data.listId + "'>" + data.prdNo + "</td>" +
								"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" + 
								"<td>" + decodeURIComponent(data.chkTy).replace(/\+/gi, " ") + "</td>" +
								"<td>" + attrTy + "</td>" + 
								"<td>" + decodeURIComponent(data.attrNameKo).replace(/\+/gi, " ") + "</td>" + 
								"<td>" + decodeURIComponent(data.spec).replace(/\+/gi, " ") + "</td>" + 
								"<td>" + data.target + "</td>" + 
								"<td>" + data.low + "</td>" + 
								"<td>" + data.up + "</td>" +
								"<td>" + data.measurer + "</td>" +
								"<td id='result" + data.id + "'>" + resultVal + "</td>" +
								"<td id='result2" + data.id + "'>" + resultVal2 + "</td>" +
								"<td id='result3" + data.id + "'>" + resultVal3 + "</td>" +
								"<td id='result4" + data.id + "'>" + resultVal4 + "</td>" +
								"<td id='fResult" + data.id + "'><select style='font-size : " + getElSize(40) + "'><option value='1'>${ok}</option><option value='2'>${faulty}</option></select></td>" +
								"<td><input type='date' value='" + date + "' style='font-size:" + getElSize(40) + "'></td>" +
								"<td id='chkCycle" + data.id + "'>" + $("#checkCycle option:selected").html() + "</td>" +
								"<td id='workTy" + data.id + "'><select style='font-size : " + getElSize(40) + "'><option value='2'>${day}</option><option value='1'>${night}</option></select></td>" +
						"</tr>";		
						
					$(".alarmTable").append(tr).css({
						"font-size": getElSize(40),
					});		
					
					var checkCycle = data.checkCycle;
					if(checkCycle==0) checkCycle = $("#checkCycle").val();
					
					$("#chkCycle" + data.id + " select option[value=" + checkCycle + "]").attr('selected','selected');
					$("#workTy" + data.id + " select option[value=" + $("#workTime").val() + "]").attr('selected','selected');
					$("#fResult" + data.id + " select option[value=" + data.fResult + "]").attr('selected','selected');
					
					if(data.attrTy==1){
						$("#result" + data.id + " select option[value=" + data.result + "]").attr('selected','selected');
						$("#result2" + data.id + " select option[value=" + data.result2 + "]").attr('selected','selected');
						$("#result3" + data.id + " select option[value=" + data.result3 + "]").attr('selected','selected');
						$("#result4" + data.id + " select option[value=" + data.result4 + "]").attr('selected','selected');
						
						if(data.result==2){
							alarmList.push("#tr" + data.id);
						}	
					}else if(Number(data.result) > Number(data.up) || Number(data.result) < Number(data.low)){
						alarmList.push("#tr" + data.id);
					}
					
					
				}
			});
			
			$(".alarmTable").append("</tbody>");
			$(".alarmTable").css("width", "100%");
			$(".alarmTable td").css({
				"padding" : getElSize(20),
				"font-size": getElSize(40),
				"border": getElSize(5) + "px solid black"
			});
			
			$("#wrapper").css({
				"height" :getElSize(1650),
				"width" : "100%",
				"overflow" : "hidden"
			});
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			$("#wrapper div:last").remove();
			scrolify($('.alarmTable'), getElSize(1450));
			$("#wrapper div:last").css("overflow", "auto");
			
			$(alarmList).each(function(idx,data){
				$(data).css("background-color" , "red");
			});
		}
	});
};

var alarmList = [];
var menu = false;
function getMousePos(){
	$(document).on("mousemove", function(e){
		var target = $("#menu_btn").width();

		if((e.pageX <= target && e.pageY <= target) || panel){
			if(!menu){
				$("#menu_btn").animate({
					"left" : 0,
					"top" : getElSize(20),
				});
				menu = true;
			};
		}else{
			if(menu){
				$("#menu_btn").animate({
					"left" : -getElSize(100),
				});	
				menu = false;
			};
		};
	});
};

var chkCycle = "<select style='font-size : " + getElSize(40) + "'><option value='0'>선택</option><option value='1'>초물</option><option value='2'>중물</option><option value='3'>종물</option></select>";
function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		url = "${ctxPath}/chart/performanceReport.do";
		location.href = url;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/main3.do";
		location.href = url;
	}else if(type=="menu4"){
		url = ctxPath + "/chart/rotateChart.do";
		location.href = url;
	}else if(type=="menu6"){
		url = "${ctxPath}/chart/toolManager.do";
		location.href = url;
	}else if(type=="menu5"){
		url = "${ctxPath}/chart/traceManager.do";
		location.href = url;
	}else if(type=="menu8"){
		url = "${ctxPath}/chart/jigGraph.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/wcGraph.do";
		location.href = url;
	}else if(type=="menu10"){
		url = "${ctxPath}/chart/addTarget.do";
		location.href = url;
	}else if(type=="menu7"){
		url = "${ctxPath}/chart/singleChartStatus.do";
		location.href = url;
	}else if(type=="menu11"){
		url = "${ctxPath}/chart/prdStatus.do";
		location.href = url;
	}else if(type=="menu99"){
		$("#bannerDiv").css({
			"z-index" : 9999
		});
		getBanner();
		closePanel();
		panel = false;
	}else if(type=="menu12"){
		url = "${ctxPath}/chart/performanceAgainstGoal.do";
		location.href = url;
	}else if(type=="menu100"){
		url = "${ctxPath}/chart/stockStatus.do";
		location.href = url;
	}else if(type=="menu101"){
		url = "${ctxPath}/chart/programManager.do";
		location.href = url;
	}else if(type=="menu102"){
		url = "${ctxPath}/chart/leadTime.do";
		location.href = url;
	}else if(type=="menu103"){
		url = "${ctxPath}/chart/faulty.do";
		location.href = url;
	}else if(type=="menu104"){
		url = "${ctxPath}/chart/addFaulty.do";
		location.href = url;
	}
};

function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};
function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url = "${ctxPath}/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			location.reload();
			//chkBanner();
		}
	});
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};


function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight, 
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$("#prdNo, #dvcId, #chkTy, #checker, #checkCycle, button, #workTime").css({
		"font-size" : getElSize(40)	
	});
	
	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});

	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#jigSelector, .goGraph, .excel, .label").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".alarmTable, .alarmTable tr, .alarmTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".alarmTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100),
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	$(".machineListForTarget").css({
		"position" : "absolute",
		"width" : getElSize(1200),
		"height" : getElSize(1200),
		"overflow" : "auto",
		//"top" : getElSize(50),
		//"background-color" : "rgb(34,34,34)",
		"background-color" : "green",
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"overflow" : "auto",
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white",
	});
	
	$(".machineListForTarget").css({
		"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	$("#table td").css({
		"padding" : getElSize(20),
		"font-size": getElSize(40),
		"border": getElSize(5) + "px solid black"
	});
	chkBanner();
};

var csvOutput;
function csvSend(){
	var sDate, eDate;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

var valueArray = [];
function saveRow(){
	valueArray = [];
	$(".contentTr").each(function(idx, data){
		var obj = new Object();
		
		obj.chkId = data.id.substr(2);
		obj.id = $(data).children("td:nth(0)").attr("id").substr(1);
		var result = $(data).children("td:nth(10)").children("input").val();
		var result2 = $(data).children("td:nth(11)").children("input").val();
		var result3 = $(data).children("td:nth(12)").children("input").val();
		var result4 = $(data).children("td:nth(13)").children("input").val();
		
		if(typeof(result)=="undefined"){
			result = $(data).children("td:nth(10)").children("select").val();
			result2 = $(data).children("td:nth(11)").children("select").val();
			result3 = $(data).children("td:nth(12)").children("select").val();
			result4 = $(data).children("td:nth(13)").children("select").val();
		}
		obj.result = result;
		obj.result2 = result2;
		obj.result3 = result3;
		obj.result4 = result4;
		
		obj.workTy = $(data).children("td:nth(17)").children("select").val();
		obj.date = $(data).children("td:nth(15)").children("input").val();
		obj.fResult = $(data).children("td:nth(14)").children("select").val(); 
		obj.checker = $("#checker").val();
		obj.chkCycle = $("#checkCycle").val(); 
			
		valueArray.push(obj);
	});
	
	var obj = new Object();
	obj.val = valueArray;
	
	var url = "${ctxPath}/chart/addCheckStandardList.do";
	var param = "val=" + JSON.stringify(obj);
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType :"text",
		success : function(data){
			if(data=="success") {
				alert ("저장 되었습니다.");
				getCheckList();
			}
		}
	});
};

function addFaulty(el){
	var prdNo = $("#prdNo").val();
	var cnt = 0;
	var url = "${ctxPath}/chart/addFaulty.do?addFaulty=true&prdNo=" + prdNo + "&cnt="+ cnt;
	
	location.href = url;
};

</script>
</head>

<body >
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>

	<div id="title_right" class="title"> </div>	
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
							<spring:message  code="f_m_l_check"></spring:message>
						</Td>
					</tr>
				</table>
				
				<div style="width: 95%">
					<div class="label" style="float: right">
					</div>
				</div>
				<center>
					<table style="color: white">
						<tr>
							<Td><spring:message  code="prd_no"></spring:message></Td><Td><select id="prdNo"></select ></Td><Td><spring:message  code="device"></spring:message></td><td ><select id="dvcId"></select></td> <Td><spring:message  code="check_type"></spring:message></Td><Td><select id="chkTy"></select></Td>
						</tr>
						<tr>
							<Td><spring:message  code="check_date"></spring:message></Td><Td><input type="date" class='date'>&nbsp; </Td><Td><spring:message  code="checker"></spring:message></Td><Td><input type="text" id="checker">  &nbsp;&nbsp;</Td><Td ><spring:message  code="check_cycle"></spring:message></Td>
							<Td>
								<select id="checkCycle"><option value="1"><spring:message  code="first_prdct"></spring:message></option><option value="2"><spring:message  code="mid_prdct"></spring:message></option><option value="3"><spring:message  code="last_prdct"></spring:message></option></select>
								<select id="workTime"><option value="2"><spring:message  code="day"></spring:message></option><option value="1"><spring:message  code="night"></spring:message></option></select>
							</Td>
						</tr>					
					</table>
				</center>
				<button style="float: left;" onclick="addFaulty()"><spring:message  code="add_faulty"></spring:message></button>
				<button style="float: right;" onclick="saveRow();"><spring:message  code="save"></spring:message></button>
				<button style="float: right;" onclick="getCheckList();"><spring:message  code="check"></spring:message></button>
				<div id="wrapper">
					<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1" id="table">
						<thead>
							<Tr style="background-color: #222222">
								<td><spring:message  code="prd_no"></spring:message></td>
								<Td><spring:message  code="device"></spring:message></Td>
								<Td><spring:message  code="check_type"></spring:message></Td>
								<Td><spring:message  code="character_type"></spring:message></Td>
								<Td>특성명(한글)</Td>
								<td><spring:message  code="drawing"></spring:message>Spec</td>
								<Td><spring:message  code="target_val"></spring:message></Td>
								<Td><spring:message  code="min_val"></spring:message></Td>
								<Td><spring:message  code="max_val"></spring:message></Td>
								<Td><spring:message  code="measurer"></spring:message></Td>
								<Td><spring:message  code="check_result"></spring:message></Td>
								<Td><spring:message  code="check_result"></spring:message></Td>
								<Td><spring:message  code="check_result"></spring:message></Td>
								<Td><spring:message  code="check_result"></spring:message></Td>
								<Td><spring:message  code="result"></spring:message></Td>
								<Td><spring:message  code="check_date"></spring:message></Td>
								<Td><spring:message  code="check_cycle"></spring:message></Td>
								<Td><spring:message  code="division"></spring:message></Td>
							</Tr>
						</thead>
					</table>
				</div>
		</div>
	</div>
</body>
</html>