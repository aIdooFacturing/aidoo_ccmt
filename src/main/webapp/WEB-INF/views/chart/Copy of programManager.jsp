<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<spring:message code="max" var="max"></spring:message>
<spring:message code="min" var="min"></spring:message>
<spring:message code="avrg" var="avrg"></spring:message>
<spring:message code="current" var="current"></spring:message>
<spring:message code="time" var="time"></spring:message>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<title><spring:message code="check_program_error"></spring:message></title>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
	}
	
	body, #container{
		overflow: hidden;
	}
</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">
var shopId = 1;

function replaceHyphen(str){
	return str.replace(/-/gi,"#");
};

var panel = false;


function drawLineChart(){
	$("#lineChart").css({
		"width" : barWidth,
	});
	
	$('#lineChart').highcharts({
        chart: {
            type: 'spline',
            //marginLeft :getElSize(70),
            "width" : barWidth,
            backgroundColor : "rgba(0,0,0,0)",
        },
        exporting : false,
        credits :false,
        title: {
            text: false
        },
        subtitle: {
            text: false
        },
        xAxis: {
            categories: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
            title: {
                text: false
            }
        },
        yAxis: {
            title: {
                text: false
            },
            min: 0
        },
        legend : {
        	verticalAlign: 'top',
        	enabled : false,
        	itemStyle : {
        			color : "white",
        			fontSize : getElSize(35)
        		}
        },
        tooltip: {
        	enabled : false,
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
        }, 

        plotOptions: {
            spline: {
                marker: {
                    enabled: false
                }
            }
        },

        series: [{
            name: 'S.L ${avrg}',
            lineWidth: getElSize(15),
           	color : "#3EC373",
            marker : {
                enabled : false,
            },

            data: SLAV
        }, {
        	name: 'S.L ${max}',
        	lineWidth: getElSize(15),
        	color : "#F2F63E",
        	 marker : {
                 enabled : false,
             },
            data: SLMX
        }, {
        	name: 'S.L ${min}',
        	lineWidth: getElSize(15),
        	color : "#F63E3E",
        	 marker : {
                 enabled : false,
             },
            data: SLMN
        }, {
        	name: 'S.L ${current}',
        	lineWidth: getElSize(15),
        	color : "#9A76AF",
        	 marker : {
                 enabled : false,
             },
            data: SLCR
        }]
    });	
};

var save_type = "init"
$(function(){
	
	setEl();
	getData();
	getDvcId();
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	$(".menu").click(goReport);
	
	$("#panel_table td").addClass("unSelected_menu");
	$("#menu101").removeClass("unSelected_menu");
	$("#menu101").addClass("selected_menu");
	
	$("#dvcId").change(getPrgCdList);
	$("#prgCd").change(getPrgInfo);
});

function getPrgCdList(){
	var url = ctxPath + "/chart/getPrgCdList.do";
	var param = "dvcId=" + $("#dvcId").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			var option = "";
			$(json).each(function(idx, data){
				option += "<option value='" + data.prgCd + "'>" + decodeURIComponent(data.prgCd).replace(/\+/gi, " ") + "</option>";
				
			});
			
			$("#prgCd").html(option);
			
			getPrgInfo();
			getData();
		}
	});
};

var maxLength;
var barWidth;
function getPrgInfo(){
	var url = ctxPath + "/chart/getPrgInfo.do";
	
	var param = "dvcId=" + $("#dvcId").val() +
				"&prgCd=" + $("#prgCd").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
		
			if(json.length==0){
				$("#lineChart").css("opacity",0);
				$("#noData").css("display","inline");
				return;
			}else{
				$("#lineChart").css("opacity",1);
				$("#noData").css("display","none");
			}
			
			maxLength = json[json.length-1].sec;
			
			barWidth = getElSize(maxLength) * 114; 
			
			SLAV = [0];
			SLMX = [0];
			SLMN = [0];
			
			$(json).each(function(idx, data){
				//SLAV.push(Number(data.slAv));
				/* SLMX.push(Number(data.slMx));
				SLMN.push(Number(data.slMn)); */
			});
				
			
			var preSec = 1;
			var preSLAV, preSLMX, preSLMN;
			var src;
			
			
			$(json).each(function(idx, data){
				if(Number(data.sec) - Number(preSec) == 0){
					preSLAV = Number(data.slAv);
					preSLMX = Number(data.slMx);
					preSLMN = Number(data.slMn);
					
					SLAV.push(preSLAV);
					SLMX.push(preSLMX);
					SLMN.push(preSLMN);
					
					preSec++;
				}else{
					var loop = Number(data.sec) - (Number(preSec));
					for(var i = 0 ; i < loop; i++){
						SLAV.push(Number(preSLAV))
						SLMX.push(Number(preSLMX))
						SLMN.push(Number(preSLMN))
					}
			
					SLAV.push(Number(data.slAv))
					SLMX.push(Number(data.slMx))
					SLMN.push(Number(data.slMn))
					
					preSec = Number(data.sec)+1;
				}
			});

			getSLCR();
		}
	});
};

function getValue(json, idx, val){
	var rtn = "";
	$(json).each(function(idx, data){
		if(idx==$(data).sec){
			rtn = $(data).val;
			return rtn;
		}
	});
	return "";
}
function getSLCR(){
	var url = ctxPath + "/chart/getSLCR.do";
	var param = "dvcId=" + $("#dvcId").val() + 
				"&prgCd=" + $("#prgCd").val();
	
	$.ajax({
		url : url,
		data : param,
		type :"post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			SLCR = [];
			$(json).each(function(idx, data){
				SLCR.push(Number(data.slCr));
			});
			
			drawLineChart();
			
			var current = json.length / maxLength;
			var currentPoint = barWidth * current - getElSize(1800);
			$("#wrapper").animate({scrollLeft: currentPoint}, 100);
				
			var pointer = document.createElement("div");
			pointer.setAttribute("id", "pointer_stick");
			pointer.style.cssText = "width : " +getElSize(10) + "px;" + 
									"height :" + ($("#lineChart").height() - getElSize(130)) +"px;" + 
									"background-color : #02A3FF;" + 
									"position:absolute;" + 
									"z-index:9999;" + 
									"top : " + ($("#lineChart").offset().top + getElSize(30)) + "px;" + 
									"left:" + (window.innerWidth/2 + getElSize(10));
			
			//$("body").append(pointer)
			
		}
	});
};

function getDvcId(){
	var url = ctxPath + "/chart/getAllDvc.do";
	var param = "shopId=" + shopId + 
				"&line=ALL";
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var option = "";
			$(json).each(function(idx, data){
				option += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
				
			});
			
			$("#dvcId").html(option);
			getPrgCdList();
		}
	});
	
};

var shopId = 1;

function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

var className = "";
var classFlag = true;

function getData(){
	clearInterval(interval);
	var url = ctxPath + "/chart/getProgCd.do";
	var param = "dvcId=" + $("#dvcId").val() + 
				"&prgCd=" + $("#prgCd").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var table = "<table>";
			
			table += "<tbody>";
			$(json).each(function(idx, data){
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				table += "<tr class='" + className + "'>" + 
							"<Td style='width:8%;text-align:center' id='l" + data.line + "'' class='line'>" + data.line + "</td>" + 
							"<Td style='text-align:left;text-align:left'>" + data.cd + "</td>" +
							"<Td style='width:10%;text-align:center'></td>" + 
							"<Td style='width:10%;text-align:center'></td>" + 
							"<Td style='width:10%;text-align:center'></td>" +
						"</tr>";
			});
			
			table += "</tbody></table>";
			
			$("#table").html(table);
			$("#table td").css({
				"color" : "white",
				"font-size" : getElSize(40),
				"padding" : getElSize(20),
				"border": getElSize(5) + "px solid rgb(50,50,50)"
			});
			
			$("#headerTable td").css({
				"color" : "white",
				"font-size" : getElSize(40),
				"padding" : getElSize(20),
				"border": getElSize(5) + "px solid rgb(50,50,50)"
			});
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			$(".tableContainer div:last").remove()
			
		//	scrolify($('#table'), getElSize(1500));
			
			$("*").not(".container, .page, #wrapper, .div").css({
				"overflow-x" : "hidden",
				"overflow-y" : "auto"
			});
			
			midTop = $(".line:nth(2)").offset().top;
			
			interval = setInterval(function(){
				getCurrentProg();	
			}, 1000);
		}
	});
};

function getToolInfo(){
	var url = ctxPath + "/chart/getToolInfo.do";
	var param = "dvcId=" + $("#dvcId").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			$("#crnTool").html(data.dataList[0].portNo);
			$("#prdCnt").html(data.dataList[0].prdCnt);
			$("#runTime").html(data.dataList[0].runTime);
			$("#limitPrdCnt").html(data.dataList[0].limitPrdCnt);
			$("#limitRunTime").html(data.dataList[0].limitRunTime);
			$("#offsetD").html(data.dataList[0].offsetD);
			$("#offsetH").html(data.dataList[0].offsetH);
			
			$("#program").html(data.dataList[0].programName);
			$("#mode").html(data.dataList[0].mode);
			$("#spdLoad").html(data.dataList[0].spdLoad);
			$("#feedOverride").html(data.dataList[0].feedOverride);
			
			$("#sec").html(data.dataList[0].sec);
			$("#avrCycleTimeSec").html(data.dataList[0].avrCycleTimeSec);
			$("#prdNo").html(data.dataList[0].prdNo);
		}
	});
};

var midTop;
var preIdx= 0;
var preDvcId = 0;
function getCurrentProg(){
	var url = ctxPath + "/chart/getCurrentProg.do";
	var param = "dvcId=" + $("#dvcId").val() + 
				"&prgCd=" + $("#prgCd").val();
	
	if(preDvcId!=$("#prgCd").val()){
		$(".tableContainer").animate({
			"scrollTop": 0
		},10)
		
		preDvcId = $("#prgCd").val();
	};
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			getToolInfo();
			var line = "l" + data.line;
			var spdLoad = data.spdLoad;
			var feedOverride = data.feedOverride;
			var time = data.sdate.substr(11, 8);
			
			var pointer = document.createElement("div");
			
			if(preIdx!=line || preDvcId!=$("#prgCd").val()){
				console.log("change")
				$("#pointer").remove();
				
				var cLine = 0;
				$(".line").each(function(idx, data){
					if(this.id == line){
						cLine = idx;
						console.log(cLine, line)
						return false;
					}
				});
		
				var top;
				
				$(".line:nth(" + (cLine) + ")").next().next().html(spdLoad);
				$(".line:nth(" + (cLine) + ")").next().next().next().html(feedOverride);
				$(".line:nth(" + (cLine) + ")").next().next().next().next().html(time);
				
				
				if(cLine>2){
					var midTop = getElSize(535)
					
					var margin =  $(".tableContainer").offset().top - $("#table").offset().top;
					var scroll = $(".line:nth(" + (cLine - 3) + ")").offset().top - getElSize(5)
					
					$(".tableContainer").animate({
						"scrollTop": margin + 	(scroll - $(".tableContainer").offset().top) + $("#headerTable").height()
					},500, function(){
						pointer.setAttribute("id", "pointer");
						pointer.style.cssText = "position : absolute;" + 
												"width:" + $("#table tr:nth(0)").width() + "px;" + 
												"height : " + $("#table tr:nth(0)").height() + "px;" + 
												"border : " + getElSize(3) + "px solid yellow;" + 
												"z-index : 9999;" + 
												"top : " + midTop + ";" +
												"left : " + $("#" + line).offset().left + ";" ;
						
						$("body").append(pointer);
					})
				}else{
					
					$(".tableContainer").animate({
						"scrollTop": 0
					},10, function(){
						top = $("#" + line).offset().top;
						
						pointer.setAttribute("id", "pointer");
						pointer.style.cssText = "position : absolute;" + 
												"width:" + $("#table tr:nth(0)").width() + "px;" + 
												"height : " + $("#table tr:nth(0)").height() + "px;" + 
												"border : " + getElSize(3) + "px solid yellow;" + 
												"z-index : 9999;" + 
												"top : " + top + ";" +
												"left : " + $("#" + line).offset().left + ";" ;
						
						$("body").append(pointer)
					})
				}
				
				preDvcId = $("#prgCd").val();
				preIdx = line;
			}
 		}
	});
};

function scroll(n){
	var cLine;
	$(".line").each(function(idx, data){
		if(this.id == "l" + n){
			cLine = idx;
			return false;
		}
	});
	
	var margin =  $(".tableContainer").offset().top - $("#table").offset().top;
	
	var scroll = $(".line:nth(" + (cLine - 3) + ")").offset().top - getElSize(5)
	
	
	$(".tableContainer").animate({
		"scrollTop": margin + 	(scroll - $(".tableContainer").offset().top)
	},500)
};

var interval = false;
var SLAV = [];
var SLMX = [];
var SLMN = [];
var SLCR = [];
function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight,
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight,
		'overflow' : "hidden"
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"padding-bottom" : getElSize(10),
		"font-size" : getElSize(100),
		"color" : "white",
		"font-weight" : "bolder"
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});


	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$("#panel_table td").addClass("unSelected_menu");

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	$("#wcList").css({
		"position" : "absolute",
		"left" : originWidth/2 + $("#wcList").width()/2,
		"top" : originHeight/2 - $("#wcList").height()/2,
		"z-index" : 9999999,
		"display" : "none"
	}); 
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(100),
		"left" : getElSize(20),
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});

	$("#panel_table td").addClass("unSelected_menu");
	
	$(".goGraph, .excel, .label, #dvcSelector, #table").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
		//"border": getElSize(5) + "px solid gray"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100)
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(40)
	});
	
	$(".chartTable").css({
		"margin-top" : getElSize(60),
		"font-size" : getElSize(50),
		//"padding" : getElSize(20),
		"border-collapse" : "collapse"
	});
	
	$(".chartTable td").css({
		"border" : "solid 1px gray"
	});
	
	
	$("#chart").css({
		"margin-bottom" : getElSize(20)
	});
	
	$("#arrow_left, #arrow_right").css({
		"width" : getElSize(100),
		"margin-left" : getElSize(50),
		"margin-right" : getElSize(50),
	});
	
	$("#table td").css({
		"color" : "white",
	});
	
	$(".tableContainer").css({
		"height" : getElSize(600),
		"width" : "95%",
		//"margin-top" : getElSize(30)
	});
	
	$(".tableContainer2").css({
		"width" : "95%",
		"margin-top" : getElSize(30)
	});
	
	$("#wrapper").css({
		"overflow-x" : "auto",
		"width" : "95%"
	});
	
	$("#lineChart").css({
		"height" : getElSize(700),
	});
	
	$("#lastDiv").css({
		"height" : getElSize(500),
		"width" : "95%",
	});
	
	$(".tableContainer, #lastDiv").css({
		"margin-left" : (contentWidth/2) -($(".tableContainer").width()/2)
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	chkBanner();
	
	$("#dvcId, #prgCd").css({
		"width" : getElSize(300),
		"height" : getElSize(70),
		"font-size" : getElSize(40)
	});
	
	$("#dvcId").css({
		"margin-left" : getElSize(110)
	});
	
	$("#toolInfoTable td, #opInfoTable td").css({
		"color" : "white",
		"font-size" : getElSize(50)
	});
	
	$("#opInfo, #toolInfo").css({
		"border" : getElSize(5) + "px solid white"
	});
	
	$("#legend table div").css({
		"height" : getElSize(20),
		"width" : getElSize(50),
		"overflow" : "hidden"
	});
	
	$("#legend table td").css({
		"font-size" : getElSize(40),
		"color" : "white"
	});
	
	$("#legend").css({
		"position" : "absolute",
		"left" : (window.innerWidth/2) - ($("#legend").width()/2),
		"z-index" : 999999
	});
	
	$("#legend").css({
		"left" : (window.innerWidth/2) - ($("#legend").width()/2),
		"top" : getElSize(970)
	});
	
	$("#noData").css({
		"color" : "white",
		"font-size" : getElSize(70),
		"padding-top" : getElSize(300),
		"position" : "absolute",
		"width" : $("#lineChart").width(),
		"height" : $("#lineChart").height(),
		"top" : $("#lineChart").offset().top,
		"left" : $("#lineChart").offset().left,
		"display" : "none"
	});
};

function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};
function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url = "${ctxPath}/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			location.reload();
			//chkBanner();
		}
	});
};

function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};



function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};

function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		url = "${ctxPath}/chart/performanceReport.do";
		location.href = url;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/main3.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/singleChartStatus2.do";
		window.localStorage.setItem("dvcId", 1)
		location.href = url;
	}else if(type=="menu4"){
		url = ctxPath + "/chart/rotateChart.do";
		location.href = url;
	}else if(type=="menu5"){
		url = "${ctxPath}/chart/traceManager.do";
		location.href = url;
	}else if(type=="menu6"){
		url = "${ctxPath}/chart/toolManager.do";
		location.href = url;
	}else if(type=="menu8"){
		url = "${ctxPath}/chart/jigGraph.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/wcGraph.do";
		location.href = url;
	}else if(type=="menu10"){
		url = "${ctxPath}/chart/addTarget.do";
		location.href = url;
	}else if(type=="menu7"){
		url = "${ctxPath}/chart/singleChartStatus.do";
		location.href = url;
	}else if(type=="menu11"){
		url = "${ctxPath}/chart/prdStatus.do";
		location.href = url;
	}else if(type=="menu99"){
		$("#bannerDiv").css({
			"z-index" : 9999
		});
		getBanner();
		closePanel();
		panel = false;
	}else if(type=="menu12"){
		url = "${ctxPath}/chart/performanceAgainstGoal.do";
		location.href = url;
	}else if(type=="menu100"){
		url = "${ctxPath}/chart/stockStatus.do";
		location.href = url;
	}else if(type=="menu101"){
		url = "${ctxPath}/chart/programManager.do";
		location.href = url;
	}else if(type=="menu102"){
		url = "${ctxPath}/chart/leadTime.do";
		location.href = url;
	}else if(type=="menu103"){
		url = "${ctxPath}/chart/faulty.do";
		location.href = url;
	}else if(type=="menu104"){
		url = "${ctxPath}/chart/addFaulty.do";
		location.href = url;
	}
};
</script>
</head>

<body>
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<div id="title_right" class="title"></div>	
	
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
				<Center>
					<div class="title"><spring:message code="check_program_error"></spring:message></div>
				</Center>
				<select id="dvcId">
					<option>장비</option>
				</select>
				
				<select id="prgCd">
					<option>프로그램</option>
				</select>
				
				<Center>
					<div class="tableContainer2">
						<table id="headerTable" style="width:100%; border-collapse: collapse;">
							<thead>
								<Tr align="center">
									<td style="width: 8%">Seq</td>
									<td>Code</td>
									<td style="width: 10%">Spindle Load</td>
									<td style="width: 10%">Feed Override</td>
									<td style="width: 10%">시간</td>
								</Tr>
							</thead>
						</table>
					</div>	
				</Center>
				
				<div class="tableContainer">
					<table id="table" style="width:100%; border-collapse: collapse;">
						<thead>
							<Tr align="center">
								<td style="width: 8%">Seq</td>
								<td>Code</td>
								<td style="width: 10%">Spindle Load</td>
								<td style="width: 10%">Feed Override</td>
								<td style="width: 10%"><spring:message code="time"></spring:message></td>
							</Tr>
						</thead>
					</table>
				</div>
				
				<div id="legend">
					<Table>
						<tr>
							<Td>
								<div style="background-color: #3EC373" class="div">&nbsp;&nbsp;&nbsp;</div>	
							</Td>
							<Td>
								<span>S.L <spring:message code="avrg"></spring:message>&nbsp;&nbsp;&nbsp;</span>	
							</Td>
							<Td>
								<div style="background-color: F2F63E"  class="div">&nbsp;&nbsp;&nbsp;</div>	
							</Td>
							<Td>
								<span>S.L <spring:message code="max"></spring:message>&nbsp;&nbsp;&nbsp;</span>	
							</Td>
							<Td>
								<div style="background-color: F63E3E" class="div">&nbsp;&nbsp;&nbsp;</div>	
							</Td>
							<Td>
								<span>S.L <spring:message code="min"></spring:message>&nbsp;&nbsp;&nbsp;</span>	
							</Td>
							<Td>
								<div style="background-color: 9A76AF" class="div">&nbsp;&nbsp;&nbsp;</div>	
							</Td>
							<Td>
								<span>S.L <spring:message code="current"></spring:message>&nbsp;&nbsp;&nbsp;</span>	
							</Td>
						</tr>
					</Table>
				</div>
				<center>
					<div id="wrapper">
						<div id="lineChart">
						</div>				
					</div>
					<div id="noData">
						<span><spring:message code="no_data"></spring:message></span>
					</div>
				</center>
				<div id="lastDiv">
					<table style="width: 100%">
						<Tr>
							<td width="50%">
								<div id="toolInfo">
									<table id="toolInfoTable" style="width: 100%">
										<tr>
											<td colspan="4" class="tbleTitle" align="center">[<spring:message code="tool_info"></spring:message>]</td>
										</tr>
										<tr>
											<td width="25%"><spring:message code="current_tool"></spring:message></td>
											<td width="25%" id="crnTool"></td>
											<td width="25%"><spring:message code="tool_status"></spring:message></td>
											<td width="25%"></td>
										</tr>
										<tr>
											<td width="25%"><spring:message code="tool_alarm"></spring:message></td>
											<td width="25%"></td>
											<td width="25%"></td>
											<td width="25%"></td>
										</tr>
										
										<tr>
											<td width="25%"><spring:message code="used_time"></spring:message></td>
											<td width="25%" id="runTime"></td>
											<td width="25%"><spring:message code="prdct_cnt"></spring:message></td>
											<td width="25%" id="prdCnt"></td>
										</tr>
										
										<tr>
											<td width="25%"><spring:message code="limit_time"></spring:message></td>
											<td width="25%" id="limitRunTime"></td>
											<td width="25%"><spring:message code="limit_cnt"></spring:message></td>
											<td width="25%" id="limitPrdCnt"></td>
										</tr>
										
										<tr>
											<td width="25%">Offset D</td>
											<td width="25%" id="offsetD"></td>
											<td width="25%">Offset H</td>
											<td width="25%" id="offsetH"></td>
										</tr>
									</table>
								</div>
							</td>
							<td>
								<div id="opInfo">
									<table id="opInfoTable" style="width: 100%">
										<tr>
											<td colspan="4" class="tbleTitle" align="center">[<spring:message code="operation_info"></spring:message>]</td>
										</tr>
										<tr>
											<td width="25%"><spring:message code="operation_prd_no"></spring:message></td>
											<td width="15%" id="prdNo"></td>
											<td width="25%">Program</td>
											<td width="15%" id="program" style="text-align: center;"></td>
										</tr>
										<tr>
											<td width="25%"><spring:message code="operation_prd_name"></spring:message></td>
											<td width="25%"></td>
											<td width="25%">Mode</td>
											<td width="25%" id="mode" style="text-align: center;"></td>
										</tr>
										
										<tr>
											<td width="25%"><spring:message code="elapse_time"></spring:message></td>
											<td width="25%" id="sec"></td>
											<td width="25%">Spindle Load</td>
											<td width="25%" id="spdLoad" style="text-align: center;"></td>
										</tr>
										
										<tr>
											<td width="25%"><spring:message code="avrg"></spring:message> Cycle Time</td>
											<td width="25%" id="avrCycleTimeSec"></td>
											<td width="25%">Feed Override</td>
											<td width="25%" id="feedOverride" style="text-align: center;"></td>
										</tr>
										<tr>
											<td width="25%"> </td>
											<td width="25%"></td>
											<td width="25%">&nbsp;</td>
											<td width="25%"></td>
										</tr>
									</table>
								</div>
							</td>
						</Tr>
					</table>
				</div>
		</div>
	</div>
</body>
</html>