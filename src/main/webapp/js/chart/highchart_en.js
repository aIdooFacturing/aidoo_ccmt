var inCycleMachine = 1; 
var waitMachine = 1;
var alarmMachine = 1;
var powerOffMachine = 1;

var operationRatio = 1;
var powerOffRatio = 1;
var currentOperating = [
                		["가동장비", inCycleMachine],
                		["대기장비", waitMachine],
                	["ALARM 장비", alarmMachine],
                		["기타 전원 Off", powerOffMachine]
                ];

var AutoMachine = [
                   			["가동율",1],
                      {name: 'Others',
                   			y:99,
                   			color : "rgba(0,0,0,0)",
                   			dataLabels: {
                   				enabled: false
                               	}
                            }
                   ];
$(function() {
	drawPieChart("pieChart1", "무인장비 가동율", AutoMachine, "%");
	drawPieChart("pieChart2","Number Of Machine's Status", currentOperating, "");

	$("#title_left").click(function(){
		location.href = ctxPath + "/chart/multiVision.do";
	});
	
	$("#title_right").click(function(){
		location.href = ctxPath + "/chart/main2.do";
	});
	
	//switch page
//	setTimeout(function(){
//		location.href = ctxPath + "/chart/multiVision.do";
//	},1000*60*5);
});

window.addEventListener("keydown", function (event) {
	if (event.keyCode == 39) {
		location.href = ctxPath + "/chart/main2.do";
	}else if (event.keyCode == 37) {
		location.href = ctxPath + "/chart/main4.do";
	};
}, true);


function drawPieChart(id, title, chartData, unit) {
	
	
	Highcharts.setOptions({
		//green yellow red black
		   colors: ['#148F01', '#C7C402', '#ff0000', '#8C9089']
	    });
	
    
    // Radialize the colors
    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: { cx: 0.5, cy: 0.5, r: 0.7 },
            stops: [
                [0, Highcharts.Color(color).brighten(0.5).get('rgb')], // darken
                [1, color]
                
            ]
        };
    });
   
	/* // Radialize the colors
    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(0.4).get('rgb')] // darken
            ]
        };
    });*/
    
	// Build the chart
	$('#' + id)
			.highcharts(
					{
						chart : {
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false,
							backgroundColor : 'rgba(255, 255, 255, 0)',
							type: 'pie',
				            options3d: {
				                enabled: true,
				                alpha: 45
				            }
						},
						credits : false,
						title : {
							text : title,
							y : getElSize(130),
							style : {
								color : "white",
								fontSize : getElSize(40),
								fontWeight : "bold"
							}
						},
						tooltip : {
							enabled : false
						},
						plotOptions : {
							pie : {
								 innerSize: getElSize(130),
								 depth: contentHeight/(targetHeight/45),
								 size:'80%',
								 cursor : 'pointer',
								 dataLabels : {
									 	enabled : true,
									 	format : '{y}' + unit,
									 	connectorColor: '#000000',
									 	distance : 2,
									 	style : {
									 		 color: 'white',
									 		 textShadow: '0px 1px 1px black',
									 		 fontSize : getElSize(30),
									}
								}
							}
						},
						exporting: false,
						series : [ {
							type : 'pie',
							data : chartData
						} ]
					});
	
	$("#pieChart2").css({
		"left" : $("#container").offset().left + getElSize(300),
		"top": $("#container").offset().top + contentHeight/(targetHeight/1480),
		"height": contentHeight/(targetHeight/700),
		"width": getElSize(750)
	});
}
